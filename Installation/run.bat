:: this file is used to assemble, link and debug an .asm file
::@echo off
tasm /zi /j %1
::pause
tlink /v %1
::@echo on
td %1