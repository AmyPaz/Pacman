IDEAL
MODEL SMALL
STACK 100h
p186
jumps

DATASEG
	;---------------------------
	;Constants.
	;---------------------------
	
	;Arrow key values.
	LEFT_KEY equ 04Bh
	RIGHT_KEY equ 4Dh
	UP_KEY equ 48h
	DOWN_KEY equ 50h
	
	;Ghosts movement constants.
	GHOST_UP_DIR equ 0h
	GHOST_DOWN_DIR equ 01h
	GHOST_LEFT_DIR equ 02h
	GHOST_RIGHT_DIR equ 03h
	
	;Sprites dimensions.
	PACMAN_LEN equ 0Ch			;The length of the pacman sprites.
	PACMAN_WIDTH equ 0Ch		;The width of the pacman sprites.
	GHOST_LEN equ 0Dh			;The length of the ghost sprites.
	GHOST_WIDTH equ 0Dh			;The width of the ghost sprites.
	
	;Background constants.
	BORDER_LINE_SIZE equ 03h
	TOP_LEFT_CORNER_X equ 10d
	TOP_LEFT_CORNER_Y equ 10d
	TOP_RIGHT_CORNER_X equ 246d
	BOTTOM_LEFT_CORNER_Y equ 180d
	DEFAULT_CORNER_LEN equ 10d
	
	;Color constants.
	RED_GHOST_COLOR equ 0Ch
	PINK_GHOST_COLOR equ 0DH
	ORANGE_GHOST_COLOR equ 06h
	CYAN_GHOST_COLOR equ 0Bh
	PACMAN_COLOR equ 0Eh
	BORDER_COLOR equ 01h
	BACKGROUND_COLOR equ 0h
	
	;Delay related constants.
	GHOSTS_SPRITES_DELAY equ 3300h
	PACMAN_SPRITES_DELAY equ 7150h
	
	;General purpose constants.
	TRUE equ 01h
	FALSE equ 0h
	EXIT_GAME equ 'q'
	
	;---------------------------	 
	;Pacman sprites.
	;---------------------------
	
	pacman_right_default	db ''	
		db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
		db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
		db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
		
	pacman_right_open db ''	
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0
			db 0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0,0
			db 0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0,0,0 
			db 0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0,0
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0,0
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
			
	pacman_left_default db ''	
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0

	pacman_left_open db ''	
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
			db 0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0,0,0,0,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
			
	pacman_up_default db ''	
			db 0,0,0,0,0,0,0,0,0,0,0,0,0
			db 0,0,0Eh,0Eh,0,0,0,0,0,0Eh,0Eh,0,0
			db 0,0Eh,0Eh,0Eh,0,0,0,0,0,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
	
	pacman_up_open db ''	
			db 0,0,0,0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0,0,0,0
			db 0Eh,0,0,0,0,0,0,0,0,0,0,0,0Eh
			db 0Eh,0Eh,0,0,0,0,0,0,0,0,0,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
	
	pacman_down_default db ''	
		db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0 
		db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
		db 0,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0
		db 0,0Eh,0Eh,0Eh,0,0,0,0,0,0Eh,0Eh,0Eh,0
		db 0,0,0Eh,0Eh,0,0,0,0,0,0Eh,0Eh,0,0
		db 0,0,0,0,0,0,0,0,0,0,0,0,0
		
	pacman_down_open db ''	
		db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
		db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0Eh,0Eh,0Eh,0Eh,0Eh,0
		db 0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0Eh,0Eh,0,0,0,0,0,0Eh,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0Eh,0,0,0,0,0,0,0,0Eh,0Eh,0Eh
		db 0Eh,0Eh,0,0,0,0,0,0,0,0,0,0Eh,0Eh
		db 0Eh,0,0,0,0,0,0,0,0,0,0,0,0Eh
		db 0,0,0,0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0,0,0,0
		db 0,0,0,0,0,0,0,0,0,0,0,0,0
		
	pacman_closed db ''	
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0
			db 0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0Eh,0,0
			db 0,0,0,0,0Eh,0Eh,0Eh,0Eh,0Eh,0,0,0,0
	
	;---------------------------
	;Ghosts sprites.
	;---------------------------
	
	red_ghost_right1 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0,0
			db 0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0
			db 0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0
			db 0,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0
			db 0,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0
			db 0Ch,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch 
			db 0Ch,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch
			db 0,0Ch,0Ch,0,0,0,0Ch,0Ch,0,0,0,0Ch,0Ch,0
	
	red_ghost_right2 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0,0
			db 0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0
			db 0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0
			db 0,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0
			db 0,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0
			db 0Ch,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch 
			db 0Ch,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch,0Ch,0Fh,0Fh,01h,01h,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0,0Ch,0Ch,0Ch,0,0,0Ch,0Ch,0Ch,0,0Ch,0Ch
			db 0Ch,0,0,0,0Ch,0Ch,0,0,0Ch,0Ch,0,0,0,0Ch
			
	red_ghost_up1 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,01h,01h,0Ch,0Ch,0Ch,0Ch,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,0Ch,0Ch,0Fh,01h,01h,0Fh,0,0
			db 0,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0
			db 0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch
			db 0,0Ch,0Ch,0,0,0,0Ch,0Ch,0,0,0,0Ch,0Ch,0
	
	red_ghost_up2 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,01h,01h,0Ch,0Ch,0Ch,0Ch,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,0Ch,0Ch,0Fh,01h,01h,0Fh,0,0
			db 0,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0
			db 0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0,0Ch,0Ch,0Ch,0,0,0Ch,0Ch,0Ch,0,0Ch,0Ch
			db 0Ch,0,0,0,0Ch,0Ch,0,0,0Ch,0Ch,0,0,0,0Ch
			
	red_ghost_down1 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0,0
			db 0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0
			db 0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0
			db 0,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0
			db 0,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0
			db 0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Fh,01h,01h,0Fh,0Ch,0Ch,0Fh,01h,01h,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Ch,01h,01h,0Ch,0Ch,0Ch,0Ch,01h,01h,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch
			db 0,0Ch,0Ch,0,0,0,0Ch,0Ch,0,0,0,0Ch,0Ch,0
	
	red_ghost_down2 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0,0
			db 0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0
			db 0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0
			db 0,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0
			db 0,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0
			db 0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Fh,01h,01h,0Fh,0Ch,0Ch,0Fh,01h,01h,0Fh,0Ch,0Ch
			db 0Ch,0Ch,0Ch,01h,01h,0Ch,0Ch,0Ch,0Ch,01h,01h,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0,0Ch,0Ch,0Ch,0,0,0Ch,0Ch,0Ch,0,0Ch,0Ch
			db 0Ch,0,0,0,0Ch,0Ch,0,0,0Ch,0Ch,0,0,0,0Ch
	
	red_ghost_left1 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0,0
			db 0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0
			db 0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0
			db 0,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0
			db 0,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0
			db 0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,0Ch 
			db 0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch,0,0Ch,0Ch,0Ch,0Ch
			db 0,0Ch,0Ch,0,0,0,0Ch,0Ch,0,0,0,0Ch,0Ch,0
	
	
	red_ghost_left2 db ''
			db 0,0,0,0,0,0Ch,0Ch,0Ch,0Ch,0,0,0,0,0
			db 0,0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0,0
			db 0,0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0,0
			db 0,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0
			db 0,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0
			db 0,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0Fh,0Fh,0Fh,0Fh,0Ch,0Ch,0
			db 0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,0Ch 
			db 0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,01h,01h,0Fh,0Fh,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch,0Fh,0Fh,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch,0Ch
			db 0Ch,0Ch,0,0Ch,0Ch,0Ch,0,0,0Ch,0Ch,0Ch,0,0Ch,0Ch
			db 0Ch,0,0,0,0Ch,0Ch,0,0,0Ch,0Ch,0,0,0,0Ch
			
	
	pink_ghost_right1 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0,0
			db 0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0
			db 0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0
			db 0,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0
			db 0,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0
			db 0Dh,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh 
			db 0Dh,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh
			db 0,0Dh,0Dh,0,0,0,0Dh,0Dh,0,0,0,0Dh,0Dh,0
	
	pink_ghost_right2 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0,0
			db 0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0
			db 0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0
			db 0,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0
			db 0,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0
			db 0Dh,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh 
			db 0Dh,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh,0Dh,0Fh,0Fh,01h,01h,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0,0Dh,0Dh,0Dh,0,0,0Dh,0Dh,0Dh,0,0Dh,0Dh
			db 0Dh,0,0,0,0Dh,0Dh,0,0,0Dh,0Dh,0,0,0,0Dh
			
	pink_ghost_up1 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,01h,01h,0Dh,0Dh,0Dh,0Dh,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,0Dh,0Dh,0Fh,01h,01h,0Fh,0,0
			db 0,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0
			db 0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh
			db 0,0Dh,0Dh,0,0,0,0Dh,0Dh,0,0,0,0Dh,0Dh,0
	
	pink_ghost_up2 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,01h,01h,0Dh,0Dh,0Dh,0Dh,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,0Dh,0Dh,0Fh,01h,01h,0Fh,0,0
			db 0,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0
			db 0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0,0Dh,0Dh,0Dh,0,0,0Dh,0Dh,0Dh,0,0Dh,0Dh
			db 0Dh,0,0,0,0Dh,0Dh,0,0,0Dh,0Dh,0,0,0,0Dh
			
	pink_ghost_down1 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0,0
			db 0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0
			db 0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0
			db 0,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0
			db 0,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0
			db 0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Fh,01h,01h,0Fh,0Dh,0Dh,0Fh,01h,01h,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,01h,01h,0Dh,0Dh,0Dh,0Dh,01h,01h,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh
			db 0,0Dh,0Dh,0,0,0,0Dh,0Dh,0,0,0,0Dh,0Dh,0
	
	pink_ghost_down2 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0,0
			db 0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0
			db 0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0
			db 0,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0
			db 0,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0
			db 0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Fh,01h,01h,0Fh,0Dh,0Dh,0Fh,01h,01h,0Fh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,01h,01h,0Dh,0Dh,0Dh,0Dh,01h,01h,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0,0Dh,0Dh,0Dh,0,0,0Dh,0Dh,0Dh,0,0Dh,0Dh
			db 0Dh,0,0,0,0Dh,0Dh,0,0,0Dh,0Dh,0,0,0,0Dh
	
	pink_ghost_left1 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0,0
			db 0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0
			db 0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0
			db 0,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0
			db 0,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0
			db 0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,0Dh 
			db 0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh,0,0Dh,0Dh,0Dh,0Dh
			db 0,0Dh,0Dh,0,0,0,0Dh,0Dh,0,0,0,0Dh,0Dh,0
	
	
	pink_ghost_left2 db ''
			db 0,0,0,0,0,0Dh,0Dh,0Dh,0Dh,0,0,0,0,0
			db 0,0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0,0
			db 0,0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0,0
			db 0,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0
			db 0,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0
			db 0,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0Fh,0Fh,0Fh,0Fh,0Dh,0Dh,0
			db 0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,0Dh 
			db 0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,01h,01h,0Fh,0Fh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh,0Fh,0Fh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh,0Dh
			db 0Dh,0Dh,0,0Dh,0Dh,0Dh,0,0,0Dh,0Dh,0Dh,0,0Dh,0Dh
			db 0Dh,0,0,0,0Dh,0Dh,0,0,0Dh,0Dh,0,0,0,0Dh
			
	
	cyan_ghost_right1 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0,0
			db 0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0
			db 0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0
			db 0,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0
			db 0,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0
			db 0Bh,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh 
			db 0Bh,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh
			db 0,0Bh,0Bh,0,0,0,0Bh,0Bh,0,0,0,0Bh,0Bh,0
	
	cyan_ghost_right2 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0,0
			db 0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0
			db 0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0
			db 0,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0
			db 0,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0
			db 0Bh,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh 
			db 0Bh,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh,0Bh,0Fh,0Fh,01h,01h,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0,0Bh,0Bh,0Bh,0,0,0Bh,0Bh,0Bh,0,0Bh,0Bh
			db 0Bh,0,0,0,0Bh,0Bh,0,0,0Bh,0Bh,0,0,0,0Bh
			
	cyan_ghost_up1 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,01h,01h,0Bh,0Bh,0Bh,0Bh,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,0Bh,0Bh,0Fh,01h,01h,0Fh,0,0
			db 0,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0
			db 0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh
			db 0,0Bh,0Bh,0,0,0,0Bh,0Bh,0,0,0,0Bh,0Bh,0
	
	cyan_ghost_up2 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,01h,01h,0Bh,0Bh,0Bh,0Bh,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,0Bh,0Bh,0Fh,01h,01h,0Fh,0,0
			db 0,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0
			db 0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0,0Bh,0Bh,0Bh,0,0,0Bh,0Bh,0Bh,0,0Bh,0Bh
			db 0Bh,0,0,0,0Bh,0Bh,0,0,0Bh,0Bh,0,0,0,0Bh
			
	cyan_ghost_down1 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0,0
			db 0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0
			db 0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0
			db 0,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0
			db 0,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0
			db 0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Fh,01h,01h,0Fh,0Bh,0Bh,0Fh,01h,01h,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,01h,01h,0Bh,0Bh,0Bh,0Bh,01h,01h,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh
			db 0,0Bh,0Bh,0,0,0,0Bh,0Bh,0,0,0,0Bh,0Bh,0
	
	cyan_ghost_down2 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0,0
			db 0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0
			db 0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0
			db 0,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0
			db 0,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0
			db 0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Fh,01h,01h,0Fh,0Bh,0Bh,0Fh,01h,01h,0Fh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,01h,01h,0Bh,0Bh,0Bh,0Bh,01h,01h,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0,0Bh,0Bh,0Bh,0,0,0Bh,0Bh,0Bh,0,0Bh,0Bh
			db 0Bh,0,0,0,0Bh,0Bh,0,0,0Bh,0Bh,0,0,0,0Bh
	
	cyan_ghost_left1 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0,0
			db 0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0
			db 0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0
			db 0,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0
			db 0,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0
			db 0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,0Bh 
			db 0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh,0,0Bh,0Bh,0Bh,0Bh
			db 0,0Bh,0Bh,0,0,0,0Bh,0Bh,0,0,0,0Bh,0Bh,0
	
	
	cyan_ghost_left2 db ''
			db 0,0,0,0,0,0Bh,0Bh,0Bh,0Bh,0,0,0,0,0
			db 0,0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0,0
			db 0,0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0,0
			db 0,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0
			db 0,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0
			db 0,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0Fh,0Fh,0Fh,0Fh,0Bh,0Bh,0
			db 0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,0Bh 
			db 0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,01h,01h,0Fh,0Fh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh,0Fh,0Fh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh,0Bh
			db 0Bh,0Bh,0,0Bh,0Bh,0Bh,0,0,0Bh,0Bh,0Bh,0,0Bh,0Bh
			db 0Bh,0,0,0,0Bh,0Bh,0,0,0Bh,0Bh,0,0,0,0Bh
			
	orange_ghost_right1 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,06h,06h,06h,06h,06h,06h,06h,06h,0,0,0
			db 0,0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0,0
			db 0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0
			db 0,06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,0
			db 0,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,0
			db 06h,06h,06h,0Fh,0Fh,01h,01h,06h,06h,0Fh,0Fh,01h,01h,06h 
			db 06h,06h,06h,0Fh,0Fh,01h,01h,06h,06h,0Fh,0Fh,01h,01h,06h
			db 06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,0,06h,06h,06h,06h,0,06h,06h,06h,06h
			db 0,06h,06h,0,0,0,06h,06h,0,0,0,06h,06h,0
	
	orange_ghost_right2 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,06h,06h,06h,06h,06h,06h,06h,06h,0,0,0
			db 0,0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0,0
			db 0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0
			db 0,06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,0
			db 0,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,0
			db 06h,06h,06h,0Fh,0Fh,01h,01h,06h,06h,0Fh,0Fh,01h,01h,06h 
			db 06h,06h,06h,0Fh,0Fh,01h,01h,06h,06h,0Fh,0Fh,01h,01h,06h
			db 06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,0,06h,06h,06h,0,0,06h,06h,06h,0,06h,06h
			db 06h,0,0,0,06h,06h,0,0,06h,06h,0,0,0,06h
			
	orange_ghost_up1 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,01h,01h,06h,06h,06h,06h,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,06h,06h,0Fh,01h,01h,0Fh,0,0
			db 0,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,0
			db 06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h
			db 06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,0,06h,06h,06h,06h,0,06h,06h,06h,06h
			db 0,06h,06h,0,0,0,06h,06h,0,0,0,06h,06h,0
	
	orange_ghost_up2 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,01h,01h,06h,06h,06h,06h,01h,01h,0,0,0
			db 0,0,0Fh,01h,01h,0Fh,06h,06h,0Fh,01h,01h,0Fh,0,0
			db 0,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,0
			db 06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h
			db 06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,0,06h,06h,06h,0,0,06h,06h,06h,0,06h,06h
			db 06h,0,0,0,06h,06h,0,0,06h,06h,0,0,0,06h
			
	orange_ghost_down1 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,06h,06h,06h,06h,06h,06h,06h,06h,0,0,0
			db 0,0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0,0
			db 0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0
			db 0,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,0
			db 0,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,0
			db 06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h
			db 06h,06h,0Fh,01h,01h,0Fh,06h,06h,0Fh,01h,01h,0Fh,06h,06h
			db 06h,06h,06h,01h,01h,06h,06h,06h,06h,01h,01h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,0,06h,06h,06h,06h,0,06h,06h,06h,06h
			db 0,06h,06h,0,0,0,06h,06h,0,0,0,06h,06h,0
	
	orange_ghost_down2 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,06h,06h,06h,06h,06h,06h,06h,06h,0,0,0
			db 0,0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0,0
			db 0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0
			db 0,06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,0
			db 0,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,0
			db 06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h
			db 06h,06h,0Fh,01h,01h,0Fh,06h,06h,0Fh,01h,01h,0Fh,06h,06h
			db 06h,06h,06h,01h,01h,06h,06h,06h,06h,01h,01h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,0,06h,06h,06h,0,0,06h,06h,06h,0,06h,06h
			db 06h,0,0,0,06h,06h,0,0,06h,06h,0,0,0,06h
	
	orange_ghost_left1 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,06h,06h,06h,06h,06h,06h,06h,06h,0,0,0
			db 0,0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0,0
			db 0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0
			db 0,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h,0
			db 0,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0
			db 06h,01h,01h,0Fh,0Fh,06h,06h,01h,01h,0Fh,0Fh,06h,06h,06h 
			db 06h,01h,01h,0Fh,0Fh,06h,06h,01h,01h,0Fh,0Fh,06h,06h,06h
			db 06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,0,06h,06h,06h,06h,0,06h,06h,06h,06h
			db 0,06h,06h,0,0,0,06h,06h,0,0,0,06h,06h,0
	
	
	orange_ghost_left2 db ''
			db 0,0,0,0,0,06h,06h,06h,06h,0,0,0,0,0
			db 0,0,0,06h,06h,06h,06h,06h,06h,06h,06h,0,0,0
			db 0,0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0,0
			db 0,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,0
			db 0,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h,0
			db 0,0Fh,0Fh,0Fh,0Fh,06h,06h,0Fh,0Fh,0Fh,0Fh,06h,06h,0
			db 06h,01h,01h,0Fh,0Fh,06h,06h,01h,01h,0Fh,0Fh,06h,06h,06h 
			db 06h,01h,01h,0Fh,0Fh,06h,06h,01h,01h,0Fh,0Fh,06h,06h,06h
			db 06h,06h,0Fh,0Fh,06h,06h,06h,06h,0Fh,0Fh,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h,06h
			db 06h,06h,0,06h,06h,06h,0,0,06h,06h,06h,0,06h,06h
			db 06h,0,0,0,06h,06h,0,0,06h,06h,0,0,0,06h
	
	;---------------------------
	;Screens	
	;---------------------------
	
	opening_screen db 0Ah, 0Dh, 0Ah, 0Dh, 0Ah, 0Dh, 0Ah, 0Dh, 0Ah, 0Dh, 0Ah, 0Dh
				  db '  8b,dPPYba,  ', 0Ah, 0Dh 
				  db '  88      "8a ', 0Ah, 0Dh 
				  db '  88      "8a ', 0Ah, 0Dh 
				  db '  88      "8a ', 0Ah, 0Dh 
				  db '  88       d8 ,adPPYYba,  ,adPPYba,  88,dPYba,,adPYba,  ,adPPYYba, 8b,dPPYba,', 0Ah, 0Dh 
				  db '  88b,   ,a8"  ""     `Y8 a8"     "" 88P*   "88"    "8a ""     `Y8 88P*   `"8a', 0Ah, 0Dh 
				  db '  88`YbbdP"*   ,adPPPPP88 8b         88      88      88 ,adPPPPP88 88       88', 0Ah, 0Dh
				  db '  88           88      88 88         88      88      88 88      88 88       88', 0Ah, 0Dh 
				  db '  88           88,    ,88 "8a,   ,aa 88      88      88 88,    ,88 88       88', 0Ah, 0Dh 
				  db '  88            "8bbdP"Y8  `"Ybbd8"* 88      88      88 `"8bbdP"Y8 88       88', 0Ah, 0Dh 
				  db 0Ah, 0Dh
				  db '		    Pacman project by Amy Paz Goldstein Alfi', 0Ah, 0Dh 
				  db 0Ah, 0Dh, 0Ah, 0Dh 
				  db '			    PRESS ANY KEY TO START', 0Ah, 0Dh, '$' 
				  
	game_over_screen db 0Ah, 0Dh, 0Ah, 0Dh
					 db '    .d8888b.', 0Ah, 0Dh
					 db '   d88P  Y88b', 0Ah, 0Dh
					 db '   888    888', 0Ah, 0Dh
					 db '   888         8888b.  88888b.d88b.   .d88b.', 0Ah, 0Dh
					 db '   888  88888     "88b 888 "888 "88b d8P  Y8b', 0Ah, 0Dh
					 db '   888    888 .d888888 888  888  888 88888888', 0Ah, 0Dh
					 db '   Y88b  d88P 888  888 888  888  888 Y8b.', 0Ah, 0Dh
					 db '    "Y8888P88 "Y888888 888  888  888  "Y8888', 0Ah, 0Dh
					 db 0Ah, 0Dh
					 db '                                        .d88888b.', 0Ah, 0Dh
					 db '                                       d88P" "Y88b', 0Ah, 0Dh
					 db '                                       888     888', 0Ah, 0Dh
					 db '                                       888     888 888  888  .d88b.  888d888', 0Ah, 0Dh
					 db '                                       888     888 888  888 d8P  Y8b 888P"', 0Ah, 0Dh
					 db '                                       888     888 Y88  88P 88888888 888', 0Ah, 0Dh
					 db '                                       Y88b. .d88P  Y8bd8P  Y8b.     888', 0Ah, 0Dh
					 db '                                        "Y88888P"    Y88P    "Y8888  888'
					 db 0Ah, 0Dh, 0Ah, 0Dh, 0Ah, 0Dh
					 db '                             PRESS ANY KEY TO EXIT', '$'
	
				  
	;---------------------------
	;Pacman related variables.
	;---------------------------
	
	pacman_x_cord db 120d		;Pacman's x cord.
	pacman_y_cord db 165d		;Pacman's y cord.
	current_pacman_sprite dw ? 	;Current pacman sprite to be drawn.
	
	;---------------------------
	;Ghosts related variables.
	;---------------------------
	
	ghost_x_cord dw ?			;The ghost's x cord.
	ghost_y_cord dw ?			;The ghost's y cord.
	current_ghost_sprite dw ? 	;Current ghost sprite to be drawn.
	ghost_dir dw ?				;The direction in which the ghost moves.
	num_of_moves_left dw ?	 	;Number of moves left to move in the chosen direction.
	
	red_ghost_x_cord db 120d
	red_ghost_y_cord db 50d
	current_red_ghost_sprite dw ?
	red_ghost_dir db ?
	red_ghost_num_of_moves_left db 0h
	
	pink_ghost_x_cord db 120d
	pink_ghost_y_cord db 100d
	current_pink_ghost_sprite dw ?
	pink_ghost_dir db ?
	pink_ghost_num_of_moves_left db 0h
	
	cyan_ghost_x_cord db 180d
	cyan_ghost_y_cord db 140d
	current_cyan_ghost_sprite dw ?
	cyan_ghost_dir db ?
	cyan_ghost_num_of_moves_left db 0h
	
	orange_ghost_x_cord db 60d
	orange_ghost_y_cord db 140d
	current_orange_ghost_sprite dw ?
	orange_ghost_dir db ?
	orange_ghost_num_of_moves_left db 0h
	;---------------------------
	;Sprites related variables.
	;---------------------------
	
	sprite_length db ?			;The length of the drawn sprite.
	sprite_width db ?			;The width of the drawn sprite.
	sprite_starting_y db ?		;The y coordinate from which the sprite will be drawn.
	sprite_ending_y db ?		;The y coordinate to which the sprite will be drawn.
	sprite_starting_x db ?		;The x coordinate from which the sprite will be drawn.
	sprite_ending_x db ?		;The x coordinate to which the sprite will be drawn.
	pixel_color db ?			;The color of the next pixel to be drawn.
	on_border db FALSE			;Whether or not the sprite hit a border when it tried to move.
	current_sprite dw ? 		;Pointer to the next sprite to be drawn/deleted.
	
	;---------------------------
	;Borders related variables.
	;---------------------------
	
	line_color dw ?
	pixel_x_cord dw ?
	pixel_y_cord dw ?
	line_len dw ? 				;The length of the line.
	line_starting_x dw ? 		;The x coordinate from which the line will be drawn.
	line_starting_y dw ? 		;The y coordinate from which the line will be drawn.
	
	;---------------------------
	;Input related variables.
	;---------------------------
	
	arrow_key_pressed db RIGHT_KEY  ;The last arrow key that the user pressed.
	user_input db ? 				;The input given by the user via the keyboard.	
	
	;---------------------------
	;General use variables
	;---------------------------
	
	rnd db ? 				;Used for generating random numbers.
	bx_saver dw ? 			;Used for generating random numbers.
	hasLost db FALSE 	 	;Whether the user has lost.
	string_to_print dw ? 	;Offset to a string to be printed.
	delay_time dw ? 		;The lower word of the number that represents the delay. Used to change the delay of the program.
	
CODESEG

;---------------------------
;General use functions.
;---------------------------

;Enters graphic mode. Screen size: 320x200.
proc graphicMode
	push ax
	mov ax, 13h		
	int 10h
	pop ax
	ret
endp graphicMode

;Enters text mode.
proc textMode
	push ax
	mov ax, 2h	
	int 10h
	pop ax
	ret
endp textMode

;Creates a delay.
;Input: "delay_time" - The lower word of the delay time. Used to change the length of the delay.
proc delay
	push cx
	push dx
	push ax
	mov cx, 0000h 			;High Word
	mov dx, [delay_time] 	;Low Word
	mov al, 0h
	mov ah, 86h 			;Wait
	int 15h
	pop ax
	pop dx
	pop cx
	ret
endp delay

;Receives input from the keyboard if it was pressed.
proc getKeyboardInput
	push ax
	mov ah, 01h
	int 16h
	jz no_key 		;If there is no char to read from the buffer.
	call readKeyboardInput
	no_key:
	pop ax
	ret
endp getKeyboardInput

;Reads the input given via the keyboard.
;Input: ah - The arrow key that was pressed.
;		al - the character that was pressed.
;Output: "arrow_key_pressed" - The arrow key that was pressed.
;	     "user_input" - The character that was pressed.
proc readKeyboardInput
	push ax
	mov ah, 0h
	int 16h
	cmp ah, UP_KEY 
	je arrow_key_was_pressed
	cmp ah, DOWN_KEY
	je arrow_key_was_pressed
	cmp ah, LEFT_KEY
	je arrow_key_was_pressed
	cmp ah, RIGHT_KEY
	jne no_arrow_key_pressed
	arrow_key_was_pressed:
		mov [arrow_key_pressed], ah  
	no_arrow_key_pressed:
		mov [user_input], al
		pop ax
		ret
endp readKeyboardInput

;Clears the keyboard buffer from excess inputs.
proc clearKeyboardBuffer
	push		ax
	push		es
	mov		ax, 0000h
	mov		es, ax
	mov		es:[041ah], 041eh
	mov		es:[041ch], 041eh	;Clears keyboard buffer.
	pop		es
	pop		ax
	ret
endp clearKeyboardBuffer

;Generates a random number between 0 and 15.
;Output: "rnd" - The random number that was generated.
PROC random
	push ax
	push bx
	push es
	mov ax, 40h	
	mov es, ax
	mov ax, [es:6Ch] ;Moves the clock's random number to ax.
	mov bx, [bx_saver]
	xor ax, [bx]
	add [bx_saver], ax
	and al, 0Fh ;Makes the number lower than 16.
	mov [rnd], al
	pop bx
	pop es
	pop ax
	ret
ENDP random

;Prints a string to the screen.
;Input: "string_to_print" - The string that will be printed.
PROC printString 
	push ax
	push dx
	mov dx, [string_to_print]
	mov ah, 09h
	int 21h 
	pop dx
	pop ax
	ret
ENDP printString

;---------------------------
;Sprites related functions.
;---------------------------

;This function sets the initial parameters of a sprite before it will be drawn.
;Input: "sprite_starting_x" - The x coordinate from which the sprite starts.
;	    "sprite_ending_y" - The y coordinate from which the sprite starts.
;		"sprite_length" - The length of the sprite.
;		"sprite_width" - The width of the sprite.
;Output: "sprite_ending_x" - The x coordinate in which the sprite ends. 
;		 "sprite_ending_y" - The y coordinate in which the sprite ends.
proc setSprite
	push ax
	mov ah, [sprite_starting_x]
	add ah, [sprite_width]
	mov [sprite_ending_x], ah
	mov ah, [sprite_starting_y]
	add ah, [sprite_length]
	mov [sprite_ending_y], ah 
	pop ax
	ret
endp setSprite

;Draws a sprite on the screen.
;Input: "current_sprite" - A pointer to the sprite to be drawn.
;		"sprite_starting_x" - The first x coordinate from which the sprite will be drawn.
;		"sprite_ending_x" - The last x coordinate in which the sprite will be drawn.
;		"sprite_starting_y" - The first y coordinate from which the sprite will be drawn.
;		"sprite_ending_x" - The last y coordinate in which the sprite will be drawn.
proc drawSprite
	push dx 
	push cx
	push ax
	push si
	
	mov si, [current_sprite]
	;Iterates over the pixels of the sprite.
	mov dl, [sprite_starting_y] 	
	draw_out_loop:
		mov cl, [sprite_starting_x]		
		draw_in_loop:
			lodsb  							;LODSB - Loads the byte at the address pointed to by SI into AL. 
			mov	[pixel_color], al		
			cmp [pixel_color], BACKGROUND_COLOR  ;If the pixel color is black, it will not be drawn.
			je 	skip_drawing_pixel
			
			mov ah, 0dh					;The pixel color is not black - reads the pixel's color.
			int 10h
			mov	al, [pixel_color]		
			mov ah, 0Ch    				;Writes the pixel.
			int 10h
			skip_drawing_pixel:
				inc cl         				;Next x coordinate.
				cmp cl, [sprite_ending_x]	;Checks for end of line.
				jbe draw_in_loop
			
		inc dl         				;Next y coordinate.
		cmp dl, [sprite_ending_y]	;Checks for end of column.
		jbe draw_out_loop
	
	pop si
	pop ax
	pop cx
	pop dx
	ret
endp drawSprite

;Deletes a sprite from the screen.
;Input: "current_sprite" - A pointer to the sprite to be deleted.
;		"sprite_starting_x" - The first x coordinate from which the sprite will be deleted.
;		"sprite_ending_x" - The last x coordinate in which the sprite will be deleted.
;		"sprite_starting_y" - The first y coordinate from which the sprite will be deleted.
;		"sprite_ending_x" - The last y coordinate in which the sprite will be deleted.
proc deleteSprite
	push dx 
	push cx
	push ax
	push si
	
	mov si, [current_sprite]
	;Iterates over the pixels of the sprite.
	mov dl, [sprite_starting_y] 	
	delete_out_loop:
		mov cl, [sprite_starting_x]		
		delete_in_loop:
			lodsb  							;LODSB - Loads the byte at the address pointed to by SI into AL. 
			mov	[pixel_color], al		
			cmp [pixel_color], BACKGROUND_COLOR ;If the pixel color is black, it will not be deleted.
			je 	skip_deleting_pixel
			
			mov ah, 0dh					;The pixel color is not black - reads the pixel's color.
			int 10h
			mov	al, BACKGROUND_COLOR
			mov ah, 0Ch    				;Deletes the pixel.
			int 10h
			skip_deleting_pixel:
				inc cl         				;Next x coordinate.
				cmp cl, [sprite_ending_x]	;Checks for end of line.
				jbe delete_in_loop
		inc dl         				;Next y coordinate.
		cmp dl, [sprite_ending_y]	;Checks for end of column.
		jbe delete_out_loop
	
	pop si
	pop ax
	pop cx
	pop dx
	ret
endp deleteSprite

;Detects a border when a sprite goes up.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.
;Output: "on_border" - Whether or not the sprite hit a border.
proc detectBorderUp
	push dx
	push cx
	push bx
	xor cx, cx
	mov [on_border], FALSE
	
	;Iterates over the top row of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_starting_y]
	mov bl, [sprite_ending_x]
	detect_up_border_loop:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call readPixel
		cmp [pixel_color], BORDER_COLOR
		jne not_on_up_border
		
		;The pixel that was read is a border.
		mov [on_border], TRUE
		not_on_up_border:
			inc cl         	;Next x coordinate.
			cmp cl, bl		;Checks for end of line.
			jbe detect_up_border_loop
	pop bx
	pop cx
	pop dx
	ret
endp detectBorderUp

;Detects a border when a sprite goes down.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.
;		"sprite_ending_y" - The last y coordinate of the sprite.		
;Output: "on_border" - Whether or not the sprite hit a border.
proc detectBorderDown
	push dx
	push cx
	push bx
	xor cx, cx
	mov [on_border], FALSE
	
	;Iterates over the bottom row of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_ending_y]
	mov bl, [sprite_ending_x]
	detect_down_border_loop:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call readPixel
		cmp [pixel_color], BORDER_COLOR
		jne not_on_down_border
		
		;The pixel that was read is a border.
		mov [on_border], TRUE
		not_on_down_border:
			inc cl         	;Next x coordinate.
			cmp cl, bl		;Checks for end of line.
			jbe detect_down_border_loop
	pop bx
	pop cx
	pop dx
	ret
endp detectBorderDown

;Detects a border when a sprite goes to the left.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_y" - The last y coordinate of the sprite.		
;Output: "on_border" - Whether or not the sprite hit a border.
proc detectBorderLeft
	push dx
	push cx
	push bx
	xor cx, cx
	mov [on_border], FALSE
	
	;Iterates over the leftmost column of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_starting_y]
	mov bl, [sprite_ending_y]
	detect_left_border_loop:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call readPixel
		cmp [pixel_color], BORDER_COLOR
		jne not_on_left_border
		
		;The pixel that was read is a border.
		mov [on_border], TRUE
		not_on_left_border:
			inc dl         	;Next x coordinate.
			cmp dl, bl		;Checks for end of column.
			jbe detect_left_border_loop
	pop bx
	pop cx
	pop dx
	ret
endp detectBorderLeft

;Detects a border when a sprite goes to the right.
;Input: "sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.		
;		"sprite_ending_y" - The last y coordinate of the sprite.		
;Output: "on_border" - Whether or not the sprite hit a border.
proc detectBorderRight
	push dx
	push cx
	push bx
	xor cx, cx
	mov [on_border], FALSE
	
	;Iterates over the rightmost column of pixels in the sprite.
	mov cl, [sprite_ending_x]
	mov dl, [sprite_starting_y]
	mov bl, [sprite_ending_y]
	detect_right_border_loop:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call readPixel
		cmp [pixel_color], BORDER_COLOR
		jne not_on_right_border
		
		;The pixel that was read is a border.
		mov [on_border], TRUE
		not_on_right_border:
			inc dl         	;Next x coordinate.
			cmp dl, bl		;Checks for end of the column.
			jbe detect_right_border_loop
	pop bx
	pop cx
	pop dx
	ret
endp detectBorderRight

;---------------------------
;Pacman related functions.
;---------------------------

;Delay between the drawing of pacman sprites.
proc pacmanSpritesDelay
	mov [delay_time], PACMAN_SPRITES_DELAY
	call delay
	ret
endp pacmanSpritesDelay

;Sets Pacman's sprite parameters.
;Input: "pacman_x_cord" - The current x coordinate of pacman.
;		"pacman_y_cord" - The current y coordinate of pacman.
;Output: "sprite_length" - The length of Pacman's sprite.
;		 "sprite_width" - The width of Pacman's sprite.
;		 "sprite_starting_x" - The current x coordinate of pacman.
;		 "sprite_starting_y" - The current y coordinate of pacman.	
proc setPacman
	push bx
	mov bh, PACMAN_LEN
	mov [sprite_length], bh
	mov bh, PACMAN_WIDTH
	mov [sprite_width], bh
	mov bh, [pacman_x_cord]
	mov [sprite_starting_x], bh
	mov bh, [pacman_y_cord]
	mov [sprite_starting_y], bh 
	call setSprite
	pop bx
	ret
endp setPacman

;Deletes a Pacman sprite from the screen.
proc deletePacman
	call setPacman
	call deleteSprite
	ret
endp deletePacman

;Checks if a pixel at given coordinates belongs to a ghost.
;If it belongs to a ghost, the user loses.
;Input - "pixel_x_cord" - The x coordinate of the pixel that will be checked.
;		 "pixel_y_cord" - The y coordinate of the pixel that will be checked.
;Output: "hasLost" - Whether or not the pixel was a ghost pixel (i.e - the user lost).
proc checkForGhostPixel
	call readPixel
	cmp [pixel_color], RED_GHOST_COLOR
	je ghost_detected
	cmp [pixel_color], PINK_GHOST_COLOR
	je ghost_detected
	cmp [pixel_color], ORANGE_GHOST_COLOR
	je ghost_detected
	cmp [pixel_color], CYAN_GHOST_COLOR
	je ghost_detected
	jmp no_ghost_detected
	ghost_detected:
		mov [hasLost], TRUE
	no_ghost_detected:
		ret
endp checkForGhostPixel

;Detects a ghost when Pacman goes up.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.
proc detectGhostsUp
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the top row of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_starting_y]
	mov bl, [sprite_ending_x]
	detect_ghost_above:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForGhostPixel
		inc cl         	;Next x coordinate.
		cmp cl, bl		;Checks for end of line.
		jbe detect_ghost_above
	pop bx
	pop cx
	pop dx
	ret
endp detectGhostsUp

;Detects a ghost when Pacman goes down.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.
;		"sprite_ending_y" - The last y coordinate of the sprite.		
proc detectGhostsDown
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the bottom row of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_ending_y]
	mov bl, [sprite_ending_x]
	detect_ghost_below:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForGhostPixel
		inc cl         	;Next x coordinate.
		cmp cl, bl		;Checks for end of line.
		jbe detect_ghost_below
	pop bx
	pop cx
	pop dx
	ret
endp detectGhostsDown

;Detects a ghost when Pacman goes to the left.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_y" - The last y coordinate of the sprite.	
proc detectGhostsLeft
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the leftmost column of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_starting_y]
	mov bl, [sprite_ending_y]
	detect_ghost_to_the_left:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForGhostPixel		
		inc dl         	;Next x coordinate.
		cmp dl, bl		;Checks for end of column.
		jbe detect_ghost_to_the_left
	pop bx
	pop cx
	pop dx
	ret
endp detectGhostsLeft

;Detects a ghost when Pacman goes to the right.
;Input: "sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.		
;		"sprite_ending_y" - The last y coordinate of the sprite.	
proc detectGhostsRight
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the rightmost column of pixels in the sprite.
	mov cl, [sprite_ending_x]
	mov dl, [sprite_starting_y]
	mov bl, [sprite_ending_y]
	detect_ghost_to_the_right:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForGhostPixel
		inc dl         	;Next x coordinate.
		cmp dl, bl		;Checks for end of the column.
		jbe detect_ghost_to_the_right
	pop bx
	pop cx
	pop dx
	ret
endp detectGhostsRight

;Checks if Pacman can move up, and if possible moves him in that direction.
proc pacmanMoveUp
	call setPacman
	dec [sprite_starting_y]
	dec [sprite_ending_y]
	call detectGhostsUp
	call detectBorderUp
	cmp [on_border], FALSE
	jne up_border_detected
	dec [pacman_y_cord]
	up_border_detected:
		ret
endp pacmanMoveUp

;Checks if Pacman can move down, and if possible moves him in that direction.
proc pacmanMoveDown
	call setPacman
	inc [sprite_starting_y]
	inc [sprite_ending_y]
	call detectGhostsDown
	call detectBorderDown 
	cmp [on_border], FALSE
	jne down_border_detected
	inc [pacman_y_cord]
	down_border_detected:
		ret
endp pacmanMoveDown

;Checks if Pacman can move to the right, and if possible moves him in that direction.
proc pacmanMoveRight
	call setPacman
	inc [sprite_starting_x]
	inc [sprite_ending_x]
	call detectBorderRight
	call detectGhostsRight
	cmp [on_border], FALSE
	jne right_border_detected
	inc [pacman_x_cord]
	right_border_detected:
		ret
endp pacmanMoveRight

;Checks if Pacman can move to the left, and if possible moves him in that direction.
proc pacmanMoveLeft
	call setPacman
	dec [sprite_starting_x]
	dec [sprite_ending_x]
	call detectBorderLeft
	call detectGhostsLeft
	cmp [on_border], FALSE
	jne left_border_detected
	dec [pacman_x_cord]
	left_border_detected:
		ret
endp pacmanMoveLeft

;Moves Pacman according to the user's input.
;Input: "arrow_key_pressed" - The arrow key that the user pressed.
proc pacmanMove
	call deleteMovePacman
	
	cmp [arrow_key_pressed], UP_KEY
	jne pacman_down
	mov [current_pacman_sprite], offset pacman_up_open
	call pacmanMoveUp
	call drawMovePacman
	call deleteMovePacman
	mov [current_pacman_sprite], offset pacman_up_default
	call pacmanMoveUp
	call drawMovePacman
	jmp pacman_end_move
	
	pacman_down:
		cmp [arrow_key_pressed], DOWN_KEY
		jne pacman_right
		mov [current_pacman_sprite], offset pacman_down_open
		call pacmanMoveDown
		call drawMovePacman
		call deleteMovePacman
		mov [current_pacman_sprite], offset pacman_down_default
		call pacmanMoveDown
		call drawMovePacman
		jmp pacman_end_move
	
	pacman_right:
		cmp [arrow_key_pressed], RIGHT_KEY
		jne pacman_left
		mov [current_pacman_sprite], offset pacman_right_open
		call pacmanMoveRight
		call drawMovePacman	
		call deleteMovePacman		
		mov [current_pacman_sprite], offset pacman_right_default
		call pacmanMoveRight
		call drawMovePacman
		jmp pacman_end_move
	
	pacman_left:
		cmp [arrow_key_pressed], LEFT_KEY
		jne pacman_end_move
		mov [current_pacman_sprite], offset pacman_left_open
		call pacmanMoveLeft
		call drawMovePacman
		call deleteMovePacman
		mov [current_pacman_sprite], offset pacman_left_default
		call pacmanMoveLeft
		call drawMovePacman
	pacman_end_move:
		ret
endp pacmanMove

;Handles the graphics of Pacman's movement.
;Input: "current_pacman_sprite" - The current pacman sprite to be drawn.
proc drawMovePacman
	push ax
	mov ax, [current_pacman_sprite]
	mov [current_sprite], ax
	call setPacman
	call drawSprite
	call pacmanSpritesDelay
	call pacmanSpritesDelay
	pop ax
	ret
endp drawMovePacman

;This function deletes Pacman's movement.
;Input: "current_pacman_sprite" - The current pacman sprite to be deleted.
proc deleteMovePacman
	push ax
	mov ax, [current_pacman_sprite]
	mov [current_sprite], ax
	call deletePacman
	pop ax
	ret
endp deleteMovePacman

;---------------------------
;Ghosts related functions.
;---------------------------

;Delay between the drawing of ghost sprites.
proc ghostsSpritesDelay
	mov [delay_time], GHOSTS_SPRITES_DELAY
	call delay
	ret
endp ghostsSpritesDelay

;Sets the ghosts' sprites parameters before they will be drawn.
;Input: "ghost_x_cord" - A pointer to the x coordinate of the cuurrent ghost.
;		"ghost_y_cord" - A pointer to the y coordinate of the cuurrent ghost.
proc setGhost
	push ax
	push bx
	mov ah, GHOST_LEN
	mov [sprite_length], ah
	mov ah, GHOST_WIDTH
	mov [sprite_width], ah
	mov bx, [ghost_x_cord]
	mov ah, [bx]
	mov [sprite_starting_x], ah
	mov bx, [ghost_y_cord]
	mov ah, [bx]
	mov [sprite_starting_y], ah 
	call setSprite
	pop bx
	pop ax
	ret
endp setGhost
 
;Clears a ghost sprite from the screen.
proc deleteGhost
	call setGhost
	call deleteSprite
	ret
endp deleteGhost

;Checks if a ghost can move up, and if possible moves it in that direction.
proc ghostMoveUp
	push bx
	call setGhost
	dec [sprite_starting_y]
	dec [sprite_ending_y]
	call detectPacmanUp
	call detectBorderUp
	cmp [on_border], FALSE
	jne up_border_detected_ghost
	mov bx, [ghost_y_cord]
	dec [bx]
	up_border_detected_ghost:
		pop bx
		ret
endp ghostMoveUp

;Checks if a ghost can move down, and if possible moves it in that direction.
proc ghostMoveDown
	push bx
	call setGhost
	inc [sprite_starting_y]
	inc [sprite_ending_y]
	call detectPacmanDown
	call detectBorderDown 
	cmp [on_border], FALSE
	jne down_border_detected_ghost
	mov bx, [ghost_y_cord]
	inc [bx]
	down_border_detected_ghost:
		pop bx
		ret
endp ghostMoveDown

;Checks if a ghost can move to the right, and if possible moves him in that direction.
proc ghostMoveRight
	push bx
	call setGhost
	inc [sprite_starting_x]
	inc [sprite_ending_x]
	call detectPacmanRight
	call detectBorderRight
	cmp [on_border], FALSE
	jne right_border_detected_ghost
	mov bx, [ghost_x_cord]
	inc [bx]
	right_border_detected_ghost:
		pop bx
		ret
endp ghostMoveRight

;Checks if a ghost can move to the left, and if possible moves him in that direction.
proc ghostMoveLeft
	push bx
	call setGhost
	dec [sprite_starting_x]
	dec [sprite_ending_x]
	call detectPacmanLeft
	call detectBorderLeft
	cmp [on_border], FALSE
	jne left_border_detected_ghost
	mov bx, [ghost_x_cord]
	dec [bx]
	left_border_detected_ghost:
		pop bx
		ret
endp ghostMoveLeft

;Handles the graphics of the ghosts' movement.
;Input: "current_ghost_sprite" - A pointer to the current ghost sprite to be drawn.
proc drawMoveGhost
	push ax
	push bx
	mov bx, [current_ghost_sprite]
	mov ax, [bx]
	mov [current_sprite], ax
	call setGhost
	call drawSprite
	call ghostsSpritesDelay
	call ghostsSpritesDelay
	pop bx
	pop ax
	ret
endp drawMoveGhost

;This function deletes the ghosts' movement.
;Input: "current_ghost_sprite" - A pointer to the current ghost sprite to be cleared.
proc deleteMoveGhost
	push ax
	mov bx, [current_ghost_sprite]
	mov ax, [bx] 
	mov [current_sprite], ax
	call deleteGhost
	pop ax
	ret
endp deleteMoveGhost

;Sets the random movement of the ghosts.
;Input: "ghost_dir" - A pointer to the variable that represent the direction in which the current 
;					  ghost is moving.
;		"num_of_moves_left" - A pointer to the variable that represent the number of moves
;							   that the current ghost has in its current direction.

proc setRandomMovement
	push ax
	push bx
	push dx
	xor dx, dx
	xor ax, ax
	xor bx, bx
	call random
	mov al, [rnd]
	mov bx, 04h
	div bx
	mov bx, [ghost_dir]
	mov [bx], al
	call random
	mov al, [rnd]
	mov bx, [num_of_moves_left]
	mov [bx], al
	add [bx], 05h
	pop dx
	pop bx
	pop ax
	ret
endp setRandomMovement

;Checks if a pixel at given coordinates belongs to Pacman.
;If it belongs to Pacman, the user loses.
;Input - "pixel_x_cord" - The x coordinate of the pixel that will be checked.
;		 "pixel_y_cord" - The y coordinate of the pixel that will be checked.
;Output: "hasLost" - Whether or not the pixel was a Pacman pixel (i.e - the user lost).
proc checkForPacmanPixel
	call readPixel
	cmp [pixel_color], PACMAN_COLOR
	jne pacman_not_detected
	mov [hasLost], TRUE
	pacman_not_detected:
		ret
endp checkForPacmanPixel

;Detects Pacman when a ghost goes up.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.
proc detectPacmanUp
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the top row of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_starting_y]
	mov bl, cl
	add bl, [sprite_width]
	detect_pacman_above:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForPacmanPixel
		inc cl         	;Next x coordinate.
		cmp cl, bl		;Checks for end of line.
		jbe detect_pacman_above
	pop bx
	pop cx
	pop dx
	ret
endp detectPacmanUp

;Detects Pacman when a ghost goes down.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.
;		"sprite_ending_y" - The last y coordinate of the sprite.		
proc detectPacmanDown
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the bottom row of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_ending_y]
	mov bl, cl
	add bl, [sprite_width]
	detect_pacman_below:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForPacmanPixel
		inc cl         	;Next x coordinate.
		cmp cl, bl		;Checks for end of line.
		jbe detect_pacman_below
	pop bx
	pop cx
	pop dx
	ret
endp detectPacmanDown

;Detects Pacman when a ghost goes to the left.
;Input: "sprite_starting_x" - The first x coordinate of the sprite.
;		"sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_y" - The last y coordinate of the sprite.	
proc detectPacmanLeft
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the leftmost column of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	mov dl, [sprite_starting_y]
	mov bl, dl
	add bl, [sprite_length]
	detect_pacman_to_the_left:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForPacmanPixel		
		inc dl         	;Next x coordinate.
		cmp dl, bl		;Checks for end of column.
		jbe detect_pacman_to_the_left
	pop bx
	pop cx
	pop dx
	ret
endp detectPacmanLeft

;Detects Pacman when a ghost goes to the right.
;Input: "sprite_starting_y" - The first y coordinate of the sprite.
;		"sprite_ending_x" - The last x coordinate of the sprite.		
;		"sprite_ending_y" - The last y coordinate of the sprite.
proc detectPacmanRight
	push dx
	push cx
	push bx
	xor cx, cx
	
	;Iterates over the rightmost column of pixels in the sprite.
	mov cl, [sprite_starting_x]		
	add cl, [sprite_width]
	mov dl, [sprite_starting_y]
	mov bl, dl
	add bl, [sprite_length]
	detect_pacman_to_the_right:
		mov [pixel_x_cord], cx
		mov [pixel_y_cord], dx
		call checkForPacmanPixel
		inc dl         	;Next x coordinate.
		cmp dl, bl		;Checks for end of the column.
		jbe detect_pacman_to_the_right
	pop bx
	pop cx
	pop dx
	ret
endp detectPacmanRight

;Moves the red ghost.
proc redGhostMove
	push ax
	
	mov ax, offset current_red_ghost_sprite
	mov [current_ghost_sprite], ax
	mov ax, offset red_ghost_dir
	mov [ghost_dir], ax
	mov ax, offset red_ghost_num_of_moves_left
	mov [num_of_moves_left], ax
	mov ax, offset red_ghost_x_cord
	mov [ghost_x_cord], ax
	mov ax, offset red_ghost_y_cord
	mov [ghost_y_cord], ax
	
	call deleteMoveGhost
	
	cmp [red_ghost_num_of_moves_left], 0h
	jge red_ghost_up
	call setRandomMovement
	red_ghost_up:
		cmp [red_ghost_dir], GHOST_UP_DIR
		jne red_ghost_down
		mov [current_red_ghost_sprite], offset red_ghost_up1
		call ghostMoveUp
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_red_ghost_sprite], offset red_ghost_up2
		call ghostMoveUp
		call drawMoveGhost
		jmp red_ghost_end_move
	
	red_ghost_down:
		cmp [red_ghost_dir], GHOST_DOWN_DIR
		jne red_ghost_right
		mov [current_red_ghost_sprite], offset red_ghost_down1
		call ghostMoveDown
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_red_ghost_sprite], offset red_ghost_down2
		call ghostMoveDown
		call drawMoveGhost
		jmp red_ghost_end_move
	
	red_ghost_right:
		cmp [red_ghost_dir], GHOST_RIGHT_DIR
		jne red_ghost_left
		mov [current_red_ghost_sprite], offset red_ghost_right1
		call ghostMoveRight
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_red_ghost_sprite], offset red_ghost_right2
		call ghostMoveRight
		call drawMoveGhost
		jmp red_ghost_end_move
	
	red_ghost_left:
		cmp [red_ghost_dir], GHOST_LEFT_DIR
		jne red_ghost_end_move
		mov [current_red_ghost_sprite], offset red_ghost_left1
		call ghostMoveLeft
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_red_ghost_sprite], offset red_ghost_left2
		call ghostMoveLeft
		call drawMoveGhost
	red_ghost_end_move:
		mov al, [red_ghost_dir]
		cmp [on_border], TRUE
		jne red_ghost_move_exit
		call setRandomMovement
		cmp al, [red_ghost_dir]
		je red_ghost_end_move
	red_ghost_move_exit:
		dec [red_ghost_num_of_moves_left]
		pop ax
		ret
endp redGhostMove

;Moves the pink ghost.
proc pinkGhostMove
	push ax
	
	mov ax, offset current_pink_ghost_sprite
	mov [current_ghost_sprite], ax
	mov ax, offset pink_ghost_dir
	mov [ghost_dir], ax
	mov ax, offset pink_ghost_num_of_moves_left
	mov [num_of_moves_left], ax
	mov ax, offset pink_ghost_x_cord
	mov [ghost_x_cord], ax
	mov ax, offset pink_ghost_y_cord
	mov [ghost_y_cord], ax
	
	call deleteMoveGhost
	
	cmp [pink_ghost_num_of_moves_left], 0h
	jge pink_ghost_up
	call setRandomMovement
	pink_ghost_up:
		cmp [pink_ghost_dir], GHOST_UP_DIR
		jne pink_ghost_down
		mov [current_pink_ghost_sprite], offset pink_ghost_up1
		call ghostMoveUp
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_pink_ghost_sprite], offset pink_ghost_up2
		call ghostMoveUp
		call drawMoveGhost
		jmp pink_ghost_end_move
	
	pink_ghost_down:
		cmp [pink_ghost_dir], GHOST_DOWN_DIR
		jne pink_ghost_right
		mov [current_pink_ghost_sprite], offset pink_ghost_down1
		call ghostMoveDown
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_pink_ghost_sprite], offset pink_ghost_down2
		call ghostMoveDown
		call drawMoveGhost
		jmp pink_ghost_end_move
	
	pink_ghost_right:
		cmp [pink_ghost_dir], GHOST_RIGHT_DIR
		jne pink_ghost_left
		mov [current_pink_ghost_sprite], offset pink_ghost_right1
		call ghostMoveRight
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_pink_ghost_sprite], offset pink_ghost_right2
		call ghostMoveRight
		call drawMoveGhost
		jmp pink_ghost_end_move
	
	pink_ghost_left:
		cmp [pink_ghost_dir], GHOST_LEFT_DIR
		jne pink_ghost_end_move
		mov [current_pink_ghost_sprite], offset pink_ghost_left1
		call ghostMoveLeft
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_pink_ghost_sprite], offset pink_ghost_left2
		call ghostMoveLeft
		call drawMoveGhost
	pink_ghost_end_move:
		mov al, [pink_ghost_dir]
		cmp [on_border], TRUE
		jne pink_ghost_move_exit
		call setRandomMovement
		cmp al, [pink_ghost_dir]
		je pink_ghost_end_move
	pink_ghost_move_exit:
		dec [pink_ghost_num_of_moves_left]
		pop ax
		ret
endp pinkGhostMove

;Moves the orange ghost.
proc orangeGhostMove
	push ax
	
	mov ax, offset current_orange_ghost_sprite
	mov [current_ghost_sprite], ax
	mov ax, offset orange_ghost_dir
	mov [ghost_dir], ax
	mov ax, offset orange_ghost_num_of_moves_left
	mov [num_of_moves_left], ax
	mov ax, offset orange_ghost_x_cord
	mov [ghost_x_cord], ax
	mov ax, offset orange_ghost_y_cord
	mov [ghost_y_cord], ax
	
	call deleteMoveGhost
	
	cmp [orange_ghost_num_of_moves_left], 0h
	jge orange_ghost_up
	call setRandomMovement
	orange_ghost_up:
		cmp [orange_ghost_dir], GHOST_UP_DIR
		jne orange_ghost_down
		mov [current_orange_ghost_sprite], offset orange_ghost_up1
		call ghostMoveUp
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_orange_ghost_sprite], offset orange_ghost_up2
		call ghostMoveUp
		call drawMoveGhost
		jmp orange_ghost_end_move
	
	orange_ghost_down:
		cmp [orange_ghost_dir], GHOST_DOWN_DIR
		jne orange_ghost_right
		mov [current_orange_ghost_sprite], offset orange_ghost_down1
		call ghostMoveDown
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_orange_ghost_sprite], offset orange_ghost_down2
		call ghostMoveDown
		call drawMoveGhost
		jmp orange_ghost_end_move
	
	orange_ghost_right:
		cmp [orange_ghost_dir], GHOST_RIGHT_DIR
		jne orange_ghost_left
		mov [current_orange_ghost_sprite], offset orange_ghost_right1
		call ghostMoveRight
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_orange_ghost_sprite], offset orange_ghost_right2
		call ghostMoveRight
		call drawMoveGhost
		jmp orange_ghost_end_move
	
	orange_ghost_left:
		cmp [orange_ghost_dir], GHOST_LEFT_DIR
		jne orange_ghost_end_move
		mov [current_orange_ghost_sprite], offset orange_ghost_left1
		call ghostMoveLeft
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_orange_ghost_sprite], offset orange_ghost_left2
		call ghostMoveLeft
		call drawMoveGhost
	orange_ghost_end_move:
		mov al, [orange_ghost_dir]
		cmp [on_border], TRUE
		jne orange_ghost_move_exit
		call setRandomMovement
		cmp al, [orange_ghost_dir]
		je orange_ghost_end_move
	orange_ghost_move_exit:
		dec [orange_ghost_num_of_moves_left]
		pop ax
		ret
endp orangeGhostMove

;Moves the cyan ghost.
proc cyanGhostMove
	push ax
	
	mov ax, offset current_cyan_ghost_sprite
	mov [current_ghost_sprite], ax
	mov ax, offset cyan_ghost_dir
	mov [ghost_dir], ax
	mov ax, offset cyan_ghost_num_of_moves_left
	mov [num_of_moves_left], ax
	mov ax, offset cyan_ghost_x_cord
	mov [ghost_x_cord], ax
	mov ax, offset cyan_ghost_y_cord
	mov [ghost_y_cord], ax
	
	call deleteMoveGhost
	
	cmp [cyan_ghost_num_of_moves_left], 0h
	jge cyan_ghost_up
	call setRandomMovement
	cyan_ghost_up:
		cmp [cyan_ghost_dir], GHOST_UP_DIR
		jne cyan_ghost_down
		mov [current_cyan_ghost_sprite], offset cyan_ghost_up1
		call ghostMoveUp
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_cyan_ghost_sprite], offset cyan_ghost_up2
		call ghostMoveUp
		call drawMoveGhost
		jmp cyan_ghost_end_move
	
	cyan_ghost_down:
		cmp [cyan_ghost_dir], GHOST_DOWN_DIR
		jne cyan_ghost_right
		mov [current_cyan_ghost_sprite], offset cyan_ghost_down1
		call ghostMoveDown
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_cyan_ghost_sprite], offset cyan_ghost_down2
		call ghostMoveDown
		call drawMoveGhost
		jmp cyan_ghost_end_move
	
	cyan_ghost_right:
		cmp [cyan_ghost_dir], GHOST_RIGHT_DIR
		jne cyan_ghost_left
		mov [current_cyan_ghost_sprite], offset cyan_ghost_right1
		call ghostMoveRight
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_cyan_ghost_sprite], offset cyan_ghost_right2
		call ghostMoveRight
		call drawMoveGhost
		jmp cyan_ghost_end_move
	
	cyan_ghost_left:
		cmp [cyan_ghost_dir], GHOST_LEFT_DIR
		jne cyan_ghost_end_move
		mov [current_cyan_ghost_sprite], offset cyan_ghost_left1
		call ghostMoveLeft
		call drawMoveGhost
		call deleteMoveGhost
		mov [current_cyan_ghost_sprite], offset cyan_ghost_left2
		call ghostMoveLeft
		call drawMoveGhost
	cyan_ghost_end_move:
		mov al, [cyan_ghost_dir]
		cmp [on_border], TRUE
		jne cyan_ghost_move_exit
		call setRandomMovement
		cmp al, [cyan_ghost_dir]
		je cyan_ghost_end_move
	cyan_ghost_move_exit:
		dec [cyan_ghost_num_of_moves_left]
		pop ax
		ret
endp cyanGhostMove

;---------------------------
;Background related functions.
;---------------------------

;Draws a pixel on the screen on given coordinates.
;Input: "pixel_x_cord" - The x coordinate in which the pixel will be drawn.
;	    "pixel_y_cord" - The y coordinate in which the pixel will be drawn.
;		"line_color" - The color of the pixel.
proc drawPixel
	push ax
	push bx
	push cx
	push dx
	xor bh, bh
	mov cx, [pixel_x_cord]
	mov dx, [pixel_y_cord]
	mov ax, [line_color]
	mov ah, 0ch
	int 10h
	pop dx
	pop cx
	pop bx
	pop ax
	ret
endp drawPixel

;Draws an horizontal line on the screen, starting 
;from given coordinates in a given length.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawHorizontalLine
	push ax
	push cx
	mov cx, [line_len]
	mov ax, [line_starting_x]
	mov [pixel_x_cord], ax
	mov ax, [line_starting_y]
	mov [pixel_y_cord], ax
	horizontal_line_loop:
		call drawPixel
		inc [pixel_x_cord]
		loop horizontal_line_loop
	pop cx
	pop ax
	ret
endp drawHorizontalLine

;Draws an horizontal line for the maze border.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawBorderHorizontalLine
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	inc [line_starting_y]
	mov [line_color], BACKGROUND_COLOR
	call drawHorizontalLine
	inc [line_starting_y]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	ret
endp drawBorderHorizontalLine

;Draws an horizontal line for the maze border at the given coordinates with a closed right end.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawClosedRightBorderHorizontalLine
	push ax
	push [line_starting_x]
	push [line_len]
	call drawBorderHorizontalLine
	mov ax, [line_len]	
	mov [line_len], 02h
	add [line_starting_x], ax
	dec [line_starting_x]
	call drawVerticalLine
	pop [line_len]
	pop [line_starting_x]
	pop ax
	ret
endp drawClosedRightBorderHorizontalLine

;Draws an horizontal line for the maze border at the given coordinates with a closed left end.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawClosedLeftBorderHorizontalLine
	push ax
	push [line_len]
	call drawBorderHorizontalLine
	mov ax, [line_len]	
	mov [line_len], 02h
	call drawVerticalLine
	pop [line_len]
	pop ax
	ret
endp drawClosedLeftBorderHorizontalLine

;Draws a vertical line on the screen, starting 
;from given coordinates in a given length.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawVerticalLine
	push ax
	push cx
	mov cx, [line_len]
	mov ax, [line_starting_y]
	mov [pixel_y_cord], ax
	mov ax, [line_starting_x]
	mov [pixel_x_cord], ax
	vertical_line_loop:
		call drawPixel
		inc [pixel_y_cord]
		loop vertical_line_loop
	pop cx
	pop ax
	ret
endp drawVerticalLine

;Draws a vertical line for the maze border.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawBorderVerticalLine
	push [line_starting_x]
	mov [line_color], BORDER_COLOR
	call drawVerticalLine
	inc [line_starting_x]
	mov [line_color], BACKGROUND_COLOR
	call drawVerticalLine
	inc [line_starting_x]
	mov [line_color], BORDER_COLOR
	call drawVerticalLine
	pop [line_starting_x]
	ret
endp drawBorderVerticalLine

;Draws a vertical line for the maze border at the given coordinates with a closed top end.
;Input: "line_starting_x" - The x coordinate from which the line will start.
;		"line_starting_y" - The y coordinate at which the line will end.
;		"line_len" - The length of the line to be drawn.
proc drawClosedTopBorderVerticalLine
	push ax
	push [line_len]
	call drawBorderVerticalLine
	mov [line_len], 02h
	call drawHorizontalLine
	pop [line_len]
	pop ax
	ret
endp drawClosedTopBorderVerticalLine

;Draws a top left corner at given coordinates. 
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderTopLeftCorner
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	call drawVerticalLine
	inc [line_starting_y]
	inc [line_starting_x]
	dec [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawHorizontalLine
	call drawVerticalLine
	inc [line_starting_y]
	inc [line_starting_x]
	dec [line_len]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	call drawverticalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	ret
endp drawBorderTopLeftCorner

;Draws a top right corner at given coordinates. 
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderTopRightCorner
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	mov [line_color], BORDER_COLOR
	;Draws the vertical lines.
	call drawVerticalLine
	dec [line_starting_x]
	inc [line_starting_y]
	dec [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawVerticalLine
	dec [line_starting_x]
	inc [line_starting_y]
	dec [line_len]
	mov [line_color], BORDER_COLOR
	call drawVerticalLine
	
	;Draws the horizontal lines.
	mov ax, [line_starting_x]
	sub ax, [line_len]
	mov [line_starting_x], ax
	call drawHorizontalLine
	dec [line_starting_y]
	inc [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawHorizontalLine
	dec [line_starting_y]
	inc [line_len]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawBorderTopRightCorner

;Draws a bottom left corner at given coordinates. 
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderBottomLeftCorner
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	mov [line_color], BORDER_COLOR
	;Draws the horizontal lines.
	call drawHorizontalLine
	dec [line_starting_y]
	inc [line_starting_x]
	dec [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawHorizontalLine
	dec [line_starting_y]
	inc [line_starting_x]
	dec [line_len]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	
	;Draws the vertical lines.
	mov ax, [line_starting_y]
	sub ax, [line_len]
	mov [line_starting_y], ax
	call drawVerticalLine
	dec [line_starting_x]
	inc [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawVerticalLine
	dec [line_starting_x]
	inc [line_len]
	mov [line_color], BORDER_COLOR
	call drawVerticalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawBorderBottomLeftCorner
  
;Draws a bottom right corner at given coordinates. 
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderBottomRightCorner
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	mov [line_color], BORDER_COLOR
	push [line_starting_y]
	push [line_starting_x]
	push [line_len]
	
	;Draws the vertical lines.
	mov ax, [line_starting_y]
	sub ax, [line_len]
	mov [line_starting_y], ax
	inc [line_len] ;???
	call drawVerticalLine
	dec [line_starting_x]
	dec [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawVerticalLine
	dec [line_starting_x]
	dec [line_len]
	mov [line_color], BORDER_COLOR
	call drawVerticalLine
	
	pop [line_len]
	pop [line_starting_x]
	pop [line_starting_y]
	
	;Draws the horizontal lines.
	mov ax, [line_starting_x]
	sub ax, [line_len]
	mov [line_starting_x], ax
	call drawHorizontalLine
	
	dec [line_starting_y]
	dec [line_len]
	mov [line_color], BACKGROUND_COLOR
	call drawHorizontalLine
	dec [line_starting_y]
	dec [line_len]
	mov [line_color], BORDER_COLOR
	call drawHorizontalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawBorderBottomRightCorner

;Draws a T shaped corner pointing down at the given coordinates.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderTCornerDown
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderTopLeftCorner
	call drawBorderTopRightCorner
	mov ax, [line_starting_x]
	sub ax, 02h
	mov [line_starting_x], ax
	mov ax, [line_starting_y]
	add ax, [line_len]
	mov [line_starting_y], ax
	mov [line_len], 05h
	call drawHorizontalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret 
endp drawBorderTCornerDown

;Draws a T shaped corner pointing down at the given coordinates with closed ends.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawClosedBorderTCornerDown
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderTCornerDown
	mov ax, [line_len]
	sub [line_starting_x], ax
	mov [line_len], 03h
	call drawVerticalLine
	add [line_starting_x], ax
	add [line_starting_x], ax
	call drawVerticalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawClosedBorderTCornerDown

;Draws a T shaped corner pointing down at the given coordinates.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderTCornerUp
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderBottomLeftCorner
	call drawBorderBottomRightCorner
	mov ax, [line_starting_x]
	sub ax, 02h
	mov [line_starting_x], ax
	mov ax, [line_starting_y]
	sub ax, [line_len]
	mov [line_starting_y], ax
	mov [line_len], 05h
	call drawHorizontalLine
	
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret 
endp drawBorderTCornerUp

;Draws a T shaped corner pointing up at the given coordinates with closed ends.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawClosedBorderTCornerUp
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderTCornerUp
	mov ax, [line_len]
	sub [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], 03h
	call drawVerticalLine
	add [line_starting_x], ax
	add [line_starting_x], ax
	call drawVerticalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawClosedBorderTCornerUp

;Draws a T shaped corner pointing to the right at the given coordinates.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderTCornerRight
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderBottomLeftCorner
	call drawBorderTopLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], 05h
	call drawVerticalLine
	
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawBorderTCornerRight

;Draws a T shaped corner pointing to the right at the given coordinates with closed ends.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawClosedBorderTCornerRight
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderTCornerRight
	mov ax, [line_len]
	sub [line_starting_y], ax
	mov [line_len], 03h
	call drawHorizontalLine
	add [line_starting_y], ax
	add [line_starting_y], ax
	call drawHorizontalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawClosedBorderTCornerRight

;Draws a T shaped corner pointing to the left at the given coordinates.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawBorderTCornerLeft
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderBottomRightCorner
	call drawBorderTopRightCorner
	
	mov ax, [line_len]
	sub [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], 05h
	call drawVerticalLine
	
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
	ret
endp drawBorderTCornerLeft

;Draws a T shaped corner pointing to the left at the given coordinates with closed ends.
;Input: "line_starting_x" - The x coordinate of the center of the corner.
;		"line_starting_y" - The y coordinate of the center of the corner.
;		"line_len" - The length of each of the ends of the corner.
proc drawClosedBorderTCornerLeft
	push ax
	push [line_starting_x]
	push [line_starting_y]
	push [line_len]
	call drawBorderTCornerLeft
	mov ax, [line_len]
	sub [line_starting_y], ax
	sub [line_starting_x], 02h
	mov [line_len], 03h
	call drawHorizontalLine
	add [line_starting_y], ax
	add [line_starting_y], ax
	call drawHorizontalLine
	pop [line_len]
	pop [line_starting_y]
	pop [line_starting_x]
	pop ax
	ret
endp drawClosedBorderTCornerLeft

;Draws the corners of the maze frame
proc drawBackgroundMazeCorners
	mov [line_starting_x], TOP_LEFT_CORNER_X
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	mov [line_starting_x], TOP_RIGHT_CORNER_X
	call drawBorderTopRightCorner
	
	mov [line_starting_x], TOP_LEFT_CORNER_X
	mov [line_starting_y], BOTTOM_LEFT_CORNER_Y
	call drawBorderBottomLeftCorner
	
	mov [line_starting_x], TOP_RIGHT_CORNER_X
	call drawBorderBottomRightCorner
	
	ret
endp drawBackgroundMazeCorners


;Draws the upper part of the maze frame.
proc drawBackgroundMazeUpperSide
	push ax
	mov ax, TOP_LEFT_CORNER_X
	add ax, TOP_RIGHT_CORNER_X
	shr ax, 01h ;Divide by 2.
	
	;ax = x position of the "cursor" that draws the background.
	mov [line_starting_x], ax
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	mov [line_len], 30d
	call drawBorderTCornerDown
	
	mov [line_starting_x], TOP_LEFT_CORNER_X
	add [line_starting_x], DEFAULT_CORNER_LEN
	mov [line_len], ax
	sub [line_len], TOP_LEFT_CORNER_X
	sub [line_len], DEFAULT_CORNER_LEN
	sub [line_len], 30d
	call drawBorderHorizontalLine
	
	mov [line_starting_x], ax
	add [line_starting_x], 30d
	mov [line_len], TOP_RIGHT_CORNER_X
	sub [line_len], DEFAULT_CORNER_LEN
	sub [line_len], 30d
	sub [line_len], ax
	call drawBorderHorizontalLine
	pop ax
	ret
endp drawBackgroundMazeUpperSide

;Draws the lower part of the maze frame.
proc drawBackgroundMazeLowerSide
	mov [line_starting_x], TOP_LEFT_CORNER_X
	add [line_starting_x], DEFAULT_CORNER_LEN
	mov [line_starting_y], BOTTOM_LEFT_CORNER_Y
	sub [line_starting_y], 02h
	mov [line_len], TOP_RIGHT_CORNER_X
	sub [line_len], DEFAULT_CORNER_LEN
	sub [line_len], DEFAULT_CORNER_LEN
	sub [line_len], TOP_LEFT_CORNER_X
	call drawBorderHorizontalLine
	ret
endp drawBackgroundMazeLowerSide

;Draws the right side of the maze frame.
proc drawBackgroundMazeRightSide
	push ax
	push bx
	push dx
	mov ax, TOP_LEFT_CORNER_Y
	add ax, BOTTOM_LEFT_CORNER_Y
	xor dx, dx
	mov bx, 03h ;End of the top third of the screen.
	div bx
	
	mov [line_starting_x], TOP_RIGHT_CORNER_X
	sub [line_starting_x], 02h
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	add [line_starting_y], DEFAULT_CORNER_LEN
	sub ax, [line_starting_y]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderVerticalLine
	
	add [line_starting_x], 02h
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderBottomRightCorner
	
	mov [line_starting_x], TOP_RIGHT_CORNER_X
	sub [line_starting_x], DEFAULT_CORNER_LEN
	sub [line_starting_x], 40d
	sub [line_starting_y], 02h
	mov ax, TOP_RIGHT_CORNER_X
	sub ax, [line_starting_x]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderHorizontalLine
	
	sub [line_starting_x], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	mov ax, [line_starting_y]
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], ax
	sub [line_len], DEFAULT_CORNER_LEN
	call drawBorderVerticalLine
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderBottomLeftCorner
	
	add [line_starting_x], DEFAULT_CORNER_LEN
	sub [line_starting_y], 02h
	mov ax, TOP_RIGHT_CORNER_X
	sub ax, [line_starting_x]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderHorizontalLine
	
	mov ax, [line_len]
	add [line_starting_x], ax
	add [line_starting_x], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopRightCorner
	
	sub [line_starting_x], 02h
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov ax, BOTTOM_LEFT_CORNER_Y
	sub ax, [line_starting_y]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderVerticalLine
	
	pop dx
	pop bx
	pop ax
	ret
endp drawBackgroundMazeRightSide

;Draws the left side of the maze frame.
proc drawBackgroundMazeLeftSide
	push ax
	push bx
	push dx
	mov ax, TOP_LEFT_CORNER_Y
	add ax, BOTTOM_LEFT_CORNER_Y
	xor dx, dx
	mov bx, 03h ;End of the top third of the screen.
	div bx
	
	mov [line_starting_x], TOP_LEFT_CORNER_X
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	add [line_starting_y], DEFAULT_CORNER_LEN
	sub ax, [line_starting_y]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderVerticalLine
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderBottomLeftCorner
	
	mov [line_starting_x], TOP_LEFT_CORNER_X
	add [line_starting_x], DEFAULT_CORNER_LEN
	sub [line_starting_y], 02h
	mov [line_len], 40d
	call drawBorderHorizontalLine
	
	mov ax, [line_len]
	add [line_starting_x], ax
	add [line_starting_x], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopRightCorner
	
	sub [line_starting_x], 02h
	mov ax, [line_starting_y]
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], ax
	sub [line_len], DEFAULT_CORNER_LEN
	call drawBorderVerticalLine
	
	add [line_starting_x], 02h
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderBottomRightCorner
	
	mov ax, [line_starting_x]
	mov [line_starting_x], TOP_LEFT_CORNER_X
	add [line_starting_x], DEFAULT_CORNER_LEN
	sub [line_starting_y], 02h
	sub ax, [line_starting_x]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderHorizontalLine
	
	mov [line_starting_x], TOP_LEFT_CORNER_X
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	add [line_starting_y], DEFAULT_CORNER_LEN
	mov ax, BOTTOM_LEFT_CORNER_Y
	sub ax, [line_starting_y]
	sub ax, DEFAULT_CORNER_LEN
	mov [line_len], ax
	call drawBorderVerticalLine
	
	pop dx
	pop bx
	pop ax
	ret
endp drawBackgroundMazeLeftSide

proc drawBackgroundMazeInternals
	push ax
	;Top left features.
	mov [line_starting_x], TOP_LEFT_CORNER_X
	add [line_starting_x], 20d
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	add [line_starting_y], 20d
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], 05h
	mov [line_len], 05h
	call drawBorderBottomLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], DEFAULT_CORNER_LEN
	add [line_len], DEFAULT_CORNER_LEN
	sub [line_len], ax
	sub [line_len], ax
	call drawBorderHorizontalLine
	
	mov ax, [line_len]
	add [line_starting_x], ax
	add [line_starting_x], 05h
	add [line_starting_y], 02h
	mov [line_len], 05h
	call drawBorderBottomRightCorner
	
	mov ax, [line_len]
	sub [line_starting_y], ax
	sub [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopRightCorner
	
	add [line_starting_x], 20d
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], 05h
	mov [line_len], 05h
	call drawBorderBottomLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], DEFAULT_CORNER_LEN
	add [line_len], DEFAULT_CORNER_LEN
	add [line_len], DEFAULT_CORNER_LEN
	sub [line_len], 05h
	sub [line_len], 05h
	call drawBorderHorizontalLine
	
	mov ax, [line_len]
	add [line_starting_x], ax
	add [line_starting_x], 05h
	add [line_starting_y], 02h
	mov [line_len], 05h
	call drawBorderBottomRightCorner
	
	mov ax, [line_len]
	sub [line_starting_y], ax
	sub [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopRightCorner
	
	mov ax, [line_len]
	sub [line_starting_x], ax
	sub [line_starting_x], DEFAULT_CORNER_LEN
	call drawBorderHorizontalLine
	
	;Top right features.	
	mov [line_starting_x], TOP_RIGHT_CORNER_X
	sub [line_starting_x], 20d
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	add [line_starting_y], 20d
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopRightCorner
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], 05h
	mov [line_len], 05h
	call drawBorderBottomRightCorner
	
	mov ax, [line_len]
	sub [line_starting_x], DEFAULT_CORNER_LEN	
	sub [line_starting_x], DEFAULT_CORNER_LEN	
	call drawBorderBottomLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], DEFAULT_CORNER_LEN
	add [line_len], DEFAULT_CORNER_LEN
	sub [line_len], ax
	sub [line_len], 05h
	call drawBorderHorizontalLine
	
	sub [line_starting_x], ax
	sub [line_starting_y], ax
	sub [line_starting_y], DEFAULT_CORNER_LEN
	add [line_starting_y], 02h
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	sub [line_starting_x], 20d
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopRightCorner
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], 05h
	mov [line_len], 05h
	call drawBorderBottomRightCorner
	
	mov ax, [line_starting_x]
	sub [line_starting_x], DEFAULT_CORNER_LEN
	sub [line_starting_x], DEFAULT_CORNER_LEN
	sub [line_starting_x], DEFAULT_CORNER_LEN
	mov [line_len], 05h
	call drawBorderBottomLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], DEFAULT_CORNER_LEN
	add [line_len], DEFAULT_CORNER_LEN
	add [line_len], DEFAULT_CORNER_LEN
	sub [line_len], ax
	sub [line_len], ax
	call drawBorderHorizontalLine
	
	sub [line_starting_x], ax
	add [line_starting_y], 02h
	sub [line_starting_y], ax
	sub [line_starting_y], DEFAULT_CORNER_LEN
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderTopLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	mov [line_len], DEFAULT_CORNER_LEN
	call drawBorderHorizontalLine	
	
	;Middle features.
	mov ax, TOP_LEFT_CORNER_X
	add ax, TOP_RIGHT_CORNER_X
	shr ax, 01h ;Divide by 2.
	mov [line_starting_x], ax
	mov [line_starting_y], TOP_LEFT_CORNER_Y
	add [line_starting_y], 60d
	mov [line_len], 20d
	call drawClosedBorderTCornerDown
	
	mov ax, [line_len]
	add [line_starting_x], ax
	add [line_starting_x], ax
	add [line_starting_x], 02h
	add [line_starting_y], ax
	call drawClosedBorderTCornerLeft
	
	sub [line_starting_x], ax
	sub [line_starting_x], ax
	sub [line_starting_x], ax
	sub [line_starting_x], ax
	sub [line_starting_x], 02h
	call drawClosedBorderTCornerRight
	
	add [line_starting_x], ax
	add [line_starting_x], ax
	add [line_starting_y], ax
	add [line_starting_y], ax
	mov [line_len], 10d
	call drawBorderTCornerDown
	mov ax, [line_len]
	add [line_starting_x], ax
	mov [line_len], 26d
	call drawClosedRightBorderHorizontalLine
	sub [line_starting_x], ax
	sub [line_starting_x], ax
	sub [line_starting_x], 25d
	call drawClosedLeftBorderHorizontalLine
	
	;Bottom part features.
	mov [line_starting_x], TOP_RIGHT_CORNER_X
	sub [line_starting_x], 25d
	mov [line_starting_y], BOTTOM_LEFT_CORNER_Y
	sub [line_starting_y], 31d
	mov [line_len], 10d
	call drawClosedTopBorderVerticalLine
	
	mov ax, [line_len]
	add [line_starting_x], 02h
	add [line_starting_y], ax
	add [line_starting_y], 02h
	mov [line_len], 04h
	call drawBorderBottomRightCorner
	
	mov ax, [line_len]
	sub [line_starting_x], ax
	sub [line_starting_x], 70d
	sub [line_starting_y], 02h
	mov [line_len], 70d
	call drawClosedLeftBorderHorizontalLine
	
	mov [line_starting_x], TOP_LEFT_CORNER_X
	add [line_starting_x], 25d
	mov [line_starting_y], BOTTOM_LEFT_CORNER_Y
	sub [line_starting_y], 31d
	mov [line_len], 10d
	call drawClosedTopBorderVerticalLine
	
	mov ax, [line_len]
	add [line_starting_y], ax
	add [line_starting_y], 02h
	mov [line_len], 04h
	call drawBorderBottomLeftCorner
	
	mov ax, [line_len]
	add [line_starting_x], ax
	sub [line_starting_y], 02h
	mov [line_len], 70d
	call drawClosedRightBorderHorizontalLine
		
	pop ax
	ret
endp drawBackgroundMazeInternals

;Draws the background maze.
proc drawBackgroundMaze
	call drawBackgroundMazeCorners
	call drawBackgroundMazeUpperSide
	call drawBackgroundMazeLowerSide
	call drawBackgroundMazeRightSide
	call drawBackgroundMazeLeftSide
	call drawBackgroundMazeInternals
	ret
endp drawBackgroundMaze

;Reads the color of a pixel at given coordinates, returns its color. 
;Input: "pixel_x_cord" - The x coordinate of the pixel that will be read.
;		"pixel_y_cord" - The y coordinate of the pixel that will be read.
;Output: "pixel_color" - The color of the pixel.
proc readPixel
	push bx
	push ax
	push cx
	push dx
	mov cx, [pixel_x_cord]
	mov dx, [pixel_y_cord]
	xor bh, bh
	mov ah, 0Dh
	int 10H 
	mov [pixel_color], al
	pop dx
	pop cx
	pop ax
	pop bx
	ret
endp readPixel

start:
	mov ax, @data
	mov ds, ax
	
	;Clears the screen.
	call graphicMode
	call textMode
	
	;Opening screen.
	mov [string_to_print], offset opening_screen
	call printString
	call readKeyboardInput
	mov [user_input], ''
	
	call graphicMode
	call drawBackgroundMaze
	
	main_loop:
		call getKeyboardInput
		call clearKeyboardBuffer
		call pacmanMove
		call redGhostMove
		call pinkGhostMove
		call orangeGhostMove
		call cyanGhostMove
		
		cmp [user_input], EXIT_GAME
		je exit
		cmp [hasLost], TRUE
		jne main_loop
	
	;Game over screen.
	call textMode
	mov [string_to_print], offset game_over_screen
	call printString
	call readKeyboardInput
	exit:
		call textMode
		mov ax, 4c00h
		int 21h
END start

;---------------------------
;Missing features:
;---------------------------
;1. Pallets and score.
;2. Ghost AI.
;3. Super pallets.
;4. Fruit
;5. Lives.
;6. Ghost cage.
;7. Teleportation tunnel.


