# Pacman

Revision of the classic game "Pacman". The program is written in x86/x64 Assembly and makes use of the graphics mode.

The current version of this project is still missing some features that are present in the original game, yet serves a good indication of the direction in which this project is headed.

## Installation

1. To run the program, please install the "DOSBox" virtual machine:
`https://sourceforge.net/projects/dosbox/files/latest/download`

2. After completing the installation process, navigate to `DOSBox 0.74 Options` in the Windows Startup Menu.

3. Add the following lines to the end of the the file that will be opened (`dosbox-0.74.conf`)

```
mount c: c:\
c:
cd tasm
cd bin
cycles = max
``` 

4. Create a folder at `C:\TASM` on your computer.
5. From the provided `.rar` file (`tasm.rar`) extract the folder `BIN` to the newly created folder `C:\TASM`.
6. Copy the provided files `pacman.asm` and `run.bat`, then place them in the folder `C:\TASM\BIN`.

## Usage
1. To run the game, simply open "DOSBox" and type:
```
run pacman
```
2. Press `F9` to execute the program.